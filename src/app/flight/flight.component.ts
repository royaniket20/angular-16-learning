import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hinv-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {

  constructor(){
    console.log(`This is called as constructor`);
    
  }
  ngOnInit(): void {
    console.log(`This is called as ngOnInit`);
}
}
