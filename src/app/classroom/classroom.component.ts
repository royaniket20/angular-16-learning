import { Component, OnInit } from '@angular/core';
import { Student } from './student';
import * as jsonData from '../../assets/STUDENT_MOCK_DATA.json';

@Component({
  selector: 'hinv-classroom',
  templateUrl: './classroom.component.html',
  styleUrls: ['./classroom.component.css'],
})
export class ClassroomComponent implements OnInit {
  studentList!: Array<Student>;
  user!: string;
  cssClass1 = 'even';
  cssClass2 = 'odd';
  cssColor = 'magenta';

  constructor() {
    this.user = 'Male';
    console.log(`Constructor has been called`);
    this.studentList = new Array();
    //Usig Fetch we have Read from Json
    fetch('../../assets/STUDENT_MOCK_DATA.json')
      .then((res) => res.json())
      .then((jsonData) => {
        jsonData.forEach((element: Student) => {
          //this.studentList.push(element);
        });
      });
    //Using Json Import
    console.log(
      `Raw Data Received from Json using Import - ${JSON.stringify(jsonData)}`
    );
    for (let index = 0; jsonData[index] != undefined; index++) {
      console.log(jsonData[index]);
      let currentStudent: Student = jsonData[index];
      this.studentList.push(currentStudent);
    }
    console.log(`End of Constructor call!!`);
    console.log(`Intial This is called ${this.cssClass1} | ${this.cssClass2}`);
  }
  ngOnInit(): void {
    console.log(this);

    console.log(`!!This is called ${this.cssClass1} | ${this.cssClass2}`);
    setInterval(this.toggleColor, 500, this);
  }

  toggleColor(context: ClassroomComponent) {
    context.cssClass1 = context.cssClass1 === 'even' ? 'odd' : 'even';
    context.cssClass2 = context.cssClass2 === 'even' ? 'odd' : 'even';
    console.log(`This is called ${context.cssClass1} | ${context.cssClass2}`);
    context.studentList[0] = context.studentList[0]; //This need or Angular to Trick the Data refresh
  }

  toggleUser() {
    switch (this.user) {
      case 'Male':
        this.user = 'Female';
        break;
      case 'Female':
        this.user = 'Trans';
        break;
      default:
        this.user = 'Male';
        break;
    }
  }
}
