import { Component } from '@angular/core';

@Component({
  selector: 'hinv-root',
  templateUrl: './app.component.html',
  // template: `<h1>Hello Worl from Inline</h1>
  //   <p>
  //     Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, nihil
  //     magni! Repellendus, necessitatibus eveniet consequatur aperiam ex optio
  //     commodi incidunt itaque. Non animi suscipit blanditiis voluptates laborum!
  //     Tempore, aliquid magnam.
  //   </p> `,
  styleUrls: ['./app.component.css'],
  /* styles: [
    `
      h1 {
        color: red;
      }
    `,
  ], */
})
export class AppComponent {
  title = 'hotelinventoryapp';
}
