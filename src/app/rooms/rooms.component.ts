import { Component } from '@angular/core';
import { Room } from './rooms';
import { NgIf } from '@angular/common';

@Component({
  selector: 'hinv-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css'],
})
export class RoomsComponent {
  hotelName!: String;
  numberOfRooms!: number;
  isRoomHidden!: boolean;
  rooms!: Room;
  constructor() {
    this.hotelName = 'Hilton Hotel';
    this.numberOfRooms = 100;
    this.isRoomHidden = false;
    this.rooms = { availableRooms: 100, bookendRooms: 50, totlRooms: 150 };
  }

  protected toggelRoomView() {
    this.isRoomHidden = !this.isRoomHidden;
    if (this.isRoomHidden) {
      this.rooms.listOfRooms = Array.from([
        'Room A1',
        'Room A2',
        'Room A3',
        'Room A4',
      ]);
    } else {
      this.rooms.listOfRooms = undefined;
    }
    this.rooms.availableRooms =this.isRoomHidden? 0 : 100;
  }
}
